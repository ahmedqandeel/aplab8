LIBS ?= -lpthread
OBJS = main.o matrix.o vector.o

prog : $(OBJS)
	g++ -o prog $(OBJS) -pthread 
matrix.o : matrix.cc matrix.h
	g++ -std=c++11 -c matrix.cc
vector.o : vector.cc vector.h
	g++ -std=c++11 -c vector.cc
main.o : main.cc vector.h matrix.h
	g++ -std=c++11 -c main.cc 
clean :
	rm $(OBJS)
		   

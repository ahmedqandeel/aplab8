#include "matrix.h"


M_Cross_N_Matrix A(1500,1500);
M_Cross_N_Matrix B(1500,1500);
vector<int> AB(2250000);

void* MatrixMatrixMultiplication(void* slice){
    long sl = (long)slice;
    if (A.col == B.row){
	 		int from = (sl * A.row)/num_thrd2; 
	  		int to = ((sl+1) * A.row)/num_thrd2;
			int sum;
			for (int i = from; i < to; i++){
				for (int j = 0; j < B.col; j++){
					sum = 0;
                    for (int k = 0; k < B.row; k++){
                        sum += (*A.vec)[(i*A.col)+k] * ((*B.vec)[j+(k*B.col)]);
                    }
					AB[(i*B.col)+j]=sum;
				}
			}
		}
    return 0;
}
